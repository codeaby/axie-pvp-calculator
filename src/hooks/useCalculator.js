import { useState } from 'react';
import { MAX_CARDS, MAX_ENERGIES } from '../constants/constants';

const useCalculator = () => {
  const [round, setRound] = useState(1);
  const [energies, setEnergies] = useState(3);
  const [cards, setCards] = useState(6);
  const [prevCards, setPrevCards] = useState([6]);
  const [fixDisabled, setFixDisabled] = useState(true);
  const [wins, setWins] = useState(0);
  const [matches, setMatches] = useState(0);

  const getNewValue = (actual, delta, max) => Math.max(0, Math.min(actual + delta, max));

  const updateEnergies = (delta) => setEnergies((e) => getNewValue(e, delta, MAX_ENERGIES));

  const updateCards = (delta) => setCards((c) => getNewValue(c, delta, MAX_CARDS));

  const resetMatch = () => {
    setRound(1);
    setEnergies(3);
    setCards(6);
    setPrevCards([6]);
    setFixDisabled(true);
  };

  const nextRound = () => {
    setPrevCards((p) => [...p, getNewValue(cards, 3, MAX_CARDS)]);
    setRound((p) => p + 1);
    updateEnergies(2);
    updateCards(3);
    setFixDisabled(false);
  };

  const fixCards = (fix) => {
    const newCards = fix - prevCards[round - 2];
    const fixedPrev = [...prevCards];
    fixedPrev[round - 2] = fix;
    fixedPrev[round - 1] = fixedPrev[round - 1] + newCards;
    setPrevCards(fixedPrev);
    updateCards(newCards);
    setFixDisabled(true);
  };

  const winRate = (rate) => {
    setWins((w) => w + rate);
    setMatches((m) => m + 1);
    resetMatch();
  };

  return {
    round,
    energies,
    cards,
    fixDisabled,
    wins,
    matches,
    resetMatch,
    nextRound,
    updateCards,
    updateEnergies,
    fixCards,
    winRate,
  };
};

export default useCalculator;
