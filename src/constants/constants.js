export const MAX_ENERGIES = 10;
export const MAX_CARDS = 14;

export const CARD_BUTTONS = [
  {
    value: '-6',
    color: 'secondary',
  },
  {
    value: '-5',
    color: 'secondary',
  },
  {
    value: '-4',
    color: 'secondary',
  },
  {
    value: '-3',
    color: 'secondary',
  },
  {
    value: '-2',
    color: 'secondary',
  },
  {
    value: '-1',
    color: 'secondary',
  },
  {
    value: '+1',
    color: 'primary',
  },
];

export const ENERGY_BUTTONS = [
  {
    value: '-6',
    color: 'secondary',
  },
  {
    value: '-5',
    color: 'secondary',
  },
  {
    value: '-4',
    color: 'secondary',
  },
  {
    value: '-3',
    color: 'secondary',
  },
  {
    value: '-2',
    color: 'secondary',
  },
  {
    value: '-1',
    color: 'secondary',
  },
  {
    value: '+1',
    color: 'primary',
  },
];

export const CARD_FIX_BUTTONS = [...Array(MAX_CARDS + 1).keys()].map((c) => ({
  value: c.toString(),
  color: 'default',
}));

export const WIN_RATE = [
  {
    value: '1',
    label: 'Win',
    color: 'primary',
  },
  {
    value: '0.5',
    label: 'Draw',
    color: 'default',
  },
  {
    value: '0',
    label: 'Loose',
    color: 'secondary',
  },
];
