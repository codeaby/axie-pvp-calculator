import React from 'react';
import { MainProviders } from 'codeaby-framework';

const Providers = ({ children }) => {
  return <MainProviders>{children}</MainProviders>;
};

export default Providers;
