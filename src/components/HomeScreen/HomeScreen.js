import { Box, Button, Grid, Typography } from '@material-ui/core';
import { Assistant } from '@material-ui/icons';
import React from 'react';
import { ENERGY_BUTTONS, WIN_RATE } from '../../constants/constants';
import useCalculator from '../../hooks/useCalculator';
import ApplicationBar from '../ApplicationBar/ApplicationBar';
import Footer from '../Footer/Footer';
import UpdateValueButtons from '../UpdateValueButtons/UpdateValueButtons';
import { CenteredColumn, Column, MainContainer, RoundLabel, ValueChip } from './HomeScreenElements';

const HomeScreen = () => {
  const { round, energies, wins, matches, resetMatch, nextRound, updateEnergies, winRate } =
    useCalculator();

  return (
    <>
      <ApplicationBar resetMatch={resetMatch} />
      <MainContainer maxWidth="xs">
        <RoundLabel>Round {round}</RoundLabel>

        <Column>
          <Grid container spacing={3} style={{ marginBottom: 12 }}>
            <CenteredColumn>
              <Box>
                <Typography align="center">Energies</Typography>
                <ValueChip>
                  <Assistant fontSize="large" /> {energies}
                </ValueChip>
              </Box>
            </CenteredColumn>
            <Grid item xs={7}>
              <UpdateValueButtons buttons={ENERGY_BUTTONS} updateValue={updateEnergies} />
            </Grid>
          </Grid>
        </Column>

        <Button
          variant="contained"
          fullWidth
          color="primary"
          onClick={nextRound}
          style={{ marginBottom: 32 }}
        >
          Next Round
        </Button>

        <Column>
          <Grid container spacing={3} style={{ marginBottom: 12 }}>
            <Grid item xs={12}>
              <Typography align="center">Win Rate</Typography>
              <Typography align="center" variant="h4">
                {wins}/{matches} ({matches > 0 ? ((wins / matches) * 100).toFixed(0) : 0}%)
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <UpdateValueButtons buttons={WIN_RATE} updateValue={(fix) => winRate(fix)} />
            </Grid>
          </Grid>
        </Column>

        <Footer />
      </MainContainer>
    </>
  );
};

export default HomeScreen;
