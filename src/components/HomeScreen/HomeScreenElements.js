import { Container, Grid, Paper, styled, Typography } from '@material-ui/core';

export const MainContainer = styled(Container)(({ theme }) => ({
  marginTop: theme.spacing(3),
  marginBottom: theme.spacing(13),
}));

export const RoundLabel = styled(Typography)(({ theme }) => ({
  margin: theme.spacing(3, 0, 3),
  textAlign: 'center',
}));

export const Column = styled(Paper)(({ theme }) => ({
  padding: theme.spacing(0, 1.5),
  marginBottom: theme.spacing(3),
}));

export const ValueChip = styled(Typography)(({ theme }) => ({
  textAlign: 'center',
  fontSize: theme.typography.h3.fontSize,
}));

export const CenteredColumn = styled(({ ...others }) => <Grid item xs={5} {...others} />)(() => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
}));
