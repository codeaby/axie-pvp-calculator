import { Box, Chip, styled, Tooltip, Typography } from '@material-ui/core';
import { AccountBalanceWallet, Favorite } from '@material-ui/icons';
import React, { useState } from 'react';

export const Donation = styled(Typography)(({ theme }) => ({
  textAlign: 'center',
  fontSize: 10,
  textTransform: 'uppercase',
  position: 'fixed',
  padding: theme.spacing(2, 0),
  bottom: 0,
  left: 0,
  right: 0,
  background: 'rgba(255,255,255,0.1)',
}));

const Footer = () => {
  const [copied, setCopied] = useState(false);

  const copyToClipboard = async (e, text) => {
    e.preventDefault();
    await navigator.clipboard.writeText(text);
    setCopied(true);
    setTimeout(() => setCopied(false), 2000);
  };

  return (
    <>
      <Box align="center" fullWidth mb={2}>
        <Typography variant="caption">v{process.env.REACT_APP_VERSION}</Typography>
      </Box>
      <Donation>
        Help me to breed{' '}
        <Favorite fontSize="small" color="secondary" style={{ verticalAlign: 'middle' }} /> at{' '}
        <Tooltip
          placement="top"
          PopperProps={{
            disablePortal: true,
          }}
          open={copied}
          disableFocusListener
          disableHoverListener
          disableTouchListener
          title="Copied to Clipboard!"
        >
          <Chip
            icon={<AccountBalanceWallet />}
            label="ronin:abee..."
            color="primary"
            size="small"
            clickable
            onClick={(e) => copyToClipboard(e, 'ronin:abee592b4803ace784d83ccbc354c89e0666d6cf')}
          />
        </Tooltip>
      </Donation>
    </>
  );
};

export default Footer;
