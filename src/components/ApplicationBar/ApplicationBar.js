import { AppBar, IconButton, Toolbar, Typography } from '@material-ui/core';
import { Replay } from '@material-ui/icons';
import React from 'react';

const ApplicationBar = ({ resetMatch }) => {
  return (
    <AppBar position="static">
      <Toolbar>
        <Typography variant="h6" style={{ flexGrow: 1 }}>
          Axie PVP Calculator
        </Typography>
        <IconButton
          aria-controls="user-menu"
          aria-haspopup="true"
          edge="end"
          color="inherit"
          onClick={resetMatch}
        >
          <Replay />
        </IconButton>
      </Toolbar>
    </AppBar>
  );
};

export default ApplicationBar;
