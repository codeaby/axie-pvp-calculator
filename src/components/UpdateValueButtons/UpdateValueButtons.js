import { Box, Button, styled } from '@material-ui/core';
import React from 'react';

const ButtonsContainer = styled(Box)(({ theme }) => ({
  display: 'flex',
  flexWrap: 'wrap',
  justifyContent: 'space-between',
  margin: theme.spacing(-1),
  columnCount: 5,
}));

const UpdateValueButton = styled(({ fullWidth, ...others }) => (
  <Button variant="contained" style={{ flex: fullWidth ? 1 : 0.5 }} {...others} />
))(({ theme }) => ({
  minWidth: 55,
  margin: theme.spacing(0.75),
  padding: theme.spacing(0.75, 0),
  fontSize: 16,
}));

const UpdateValueButtons = ({ updateValue, buttons, disabled = false }) => {
  return (
    <ButtonsContainer>
      {buttons.map(({ value, color, label }) => (
        <UpdateValueButton
          color={color}
          fullWidth={value === '+1'}
          onClick={() => updateValue(parseFloat(value))}
          disabled={disabled}
        >
          {label || value}
        </UpdateValueButton>
      ))}
    </ButtonsContainer>
  );
};

export default UpdateValueButtons;
